package com.javliin.potionstack;

import com.gmail.nossr50.config.skills.alchemy.PotionConfig;
import com.gmail.nossr50.datatypes.skills.alchemy.AlchemyPotion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PotionStack extends JavaPlugin implements Listener, CommandExecutor
{

	private Map<String, String> messages    = new HashMap<>();
	private Map<UUID, Boolean>  potionstack = new HashMap<>();

	private FileConfiguration saves;

	public void onEnable()
	{
		getServer().getPluginManager().registerEvents(this, this);
		getCommand("pot").setExecutor(this);

		saveDefaultConfig();
		setMessages();

		File file = new File(getDataFolder(), "states.yml");

		if (!file.exists())
		{
			try
			{
				file.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		this.saves = YamlConfiguration.loadConfiguration(file);
	}

	@Override
	public void onDisable()
	{
		try
		{
			saves.save(new File(getDataFolder(), "states.yml"));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void setMessages()
	{
		messages.put("no_permission", (String) getConfig().get("message.no-permission"));
		messages.put("send_in_game", (String) getConfig().get("message.send-in-game"));
		messages.put("potion_toggle", (String) getConfig().get("message.toggle-potion-stack"));
		messages.put("potion_stacked", (String) getConfig().get("message.potions-stacked"));
		messages.put("usage", (String) getConfig().get("message.usage"));
		messages.put("help", (String) getConfig().get("message.help"));

		for (String name : messages.keySet())
			messages.put(name, ChatColor.translateAlternateColorCodes('&', messages.get(name)));
	}

	private Inventory replacePotions(Inventory inv)
	{
		Map<AlchemyPotion, Integer> mcmmo  = new HashMap<>();
		Map<Short, Integer>         normal = new HashMap<>();

		ItemStack[] c = inv.getContents();
		for (ItemStack stack : c)
		{
			if (stack != null && stack.getType() == Material.POTION)
			{
				Short         du = stack.getDurability();
				AlchemyPotion a  = PotionConfig.getInstance().getPotion(du);
				if (a != null)
				{
					if (mcmmo.containsKey(a)) mcmmo.replace(a, mcmmo.get(a) + stack.getAmount());
					else mcmmo.put(a, stack.getAmount());
					inv.remove(stack);
				} else
				{
					if (normal.containsKey(du)) normal.replace(du, normal.get(du) + stack.getAmount());
					else normal.put(du, stack.getAmount());
					inv.remove(stack);
				}
			}
		}

		int maxStack = getConfig().getInt("max");

		for (AlchemyPotion ap : mcmmo.keySet())
		{
			if (mcmmo.get(ap) > maxStack)
			{
				int timesOver = (int) Math.floor(mcmmo.get(ap) / maxStack);
				for (int i = 0; i < timesOver; i++) inv.addItem(ap.toItemStack(maxStack));
				int leftOver = mcmmo.get(ap) - (timesOver * maxStack);
				if (leftOver > 0) inv.addItem(ap.toItemStack(leftOver));
			} else inv.addItem(ap.toItemStack(mcmmo.get(ap)));
		}

		for (Short s : normal.keySet())
		{
			if (normal.get(s) > maxStack)
			{
				int timesOver = (int) Math.floor(normal.get(s) / maxStack);
				for (int i = 0; i < timesOver; i++) inv.addItem(new ItemStack(Material.POTION, maxStack, s));
				int leftOver = normal.get(s) - (timesOver * maxStack);
				if (leftOver > 0) inv.addItem(new ItemStack(Material.POTION, leftOver, s));
			} else inv.addItem(new ItemStack(Material.POTION, normal.get(s), s));
		}

		normal.clear();
		mcmmo.clear();

		return inv;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if (!(sender instanceof Player))
		{
			sender.sendMessage(messages.get("send_in_game"));
			return true;
		}

		if (!sender.hasPermission("potionstack.use"))
		{
			sender.sendMessage(messages.get("no_permission"));
			return true;
		}

		Player player = ((Player) sender);

		if (args.length > 0 && args[0].equalsIgnoreCase("auto"))
		{
			if (!sender.hasPermission("potionstack.use.auto"))
			{
				sender.sendMessage(messages.get("no_permission"));
				return true;
			}

			if (potionstack.get(player.getUniqueId()) != null)
			{
				potionstack.put(player.getUniqueId(), !potionstack.get(player.getUniqueId()));
			} else
			{
				potionstack.put(player.getUniqueId(), true);
			}

			if (potionstack.get(player.getUniqueId()))
				replacePotions(player.getInventory());

			sender.sendMessage(messages.get("potion_toggle").replace("{{state}}", potionstack.get(player.getUniqueId()) ? "enabled" : "disabled"));
		} else if (args.length > 0 && args[0].equalsIgnoreCase("help"))
		{
			sender.sendMessage(messages.get("help"));
		} else if (args.length > 0)
		{
			sender.sendMessage(messages.get("usage"));
		} else
		{
			replacePotions(player.getInventory());
			sender.sendMessage(messages.get("potion_stacked"));
		}

		return true;
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent event)
	{
		Player player = event.getPlayer();

		if (!(potionstack.get(player.getUniqueId()) != null && potionstack.get(player.getUniqueId())))
			return;

		Bukkit.getScheduler().scheduleSyncDelayedTask(this, () ->
		{
			replacePotions(player.getInventory());
			player.updateInventory();
		}, 2);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		if (saves.isSet(event.getPlayer().getUniqueId().toString()))
		{
			potionstack.put(event.getPlayer().getUniqueId(), saves.getBoolean(event.getPlayer().getUniqueId().toString()));
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerJoinEvent event)
	{
		if (potionstack.containsKey(event.getPlayer().getUniqueId()))
		{
			saves.set(event.getPlayer().getUniqueId().toString(), potionstack.get(event.getPlayer().getUniqueId()));
		}
	}

}
